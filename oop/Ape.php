<?php
require_once "Animal.php";

class Ape extends Animal
{
   public function yell()
   {
      echo "Yell : Auooo<br>";
   }

   public function get_legs()
   {
      return 2;
   }
}
